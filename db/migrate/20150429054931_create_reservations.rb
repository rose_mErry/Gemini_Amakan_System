class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :fullname, null: false
      t.string :address
      t.string :email
      t.string :password
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
