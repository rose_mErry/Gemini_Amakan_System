json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :fullname, :address, :email, :password, :quantity
  json.url reservation_url(reservation, format: :json)
end
