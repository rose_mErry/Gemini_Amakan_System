json.array!(@comments) do |comment|
  json.extract! comment, :id, :commenter, :article
  json.url comment_url(comment, format: :json)
end
