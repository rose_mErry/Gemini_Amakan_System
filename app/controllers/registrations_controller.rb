class RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters, if: :devise_controller?

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update).push(:lastname,:middlename, :firstname)
    devise_parameter_sanitizer.for(:sign_up).push(:lastname,:middlename, :firstname)
  end
end